use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber::fmt::Subscriber;

#[derive(Deserialize)]
struct Request {
    squared_sum: i32
}

#[derive(Serialize)]
struct Response {
    message: String
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Use the squared_sum from the request to determine the response message.
    let message = format!("The square of the sum is {}", event.payload.squared_sum);

    let resp = Response {
        message,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Setup the tracing subscriber for logging
    Subscriber::builder()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    run(service_fn(function_handler)).await
}
