use aws_sdk_dynamodb::{Client, model::AttributeValue};
use aws_config::from_env as aws_config_from_env;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber::fmt::Subscriber;
use tracing_subscriber::fmt;

#[derive(Deserialize)]
struct Request {
    sum: i32
}

#[derive(Serialize)]
struct Response {
    squared_sum: i32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Calculate square of the sum
    let squared_sum = event.payload.sum * event.payload.sum;

    // Initialize AWS SDK config
    let config = aws_config_from_env().load().await;
    let client = Client::new(&config);

    // Prepare DynamoDB item
    let sum_value = AttributeValue::N(squared_sum.to_string());
    let _resp = client
        .put_item()
        .table_name("sums")
        .item("squared_sum", sum_value)
        .send()
        .await?;

    // Prepare and send response
    let resp = Response {
        squared_sum,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    fmt::Subscriber::builder().init();
    run(service_fn(function_handler)).await
}
