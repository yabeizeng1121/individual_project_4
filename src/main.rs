use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    number1: i32,
    number2: i32,
}

#[derive(Serialize)]
struct Response {
    sum: i32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let sum = event.payload.number1 + event.payload.number2;

    let resp = Response {
        sum,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(function_handler)).await
}
