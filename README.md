# Rust AWS Lambda and Step Functions Project

Yabei Zeng

## Project Overview
This project demonstrates the use of Rust for AWS Lambda functions and AWS Step Functions to orchestrate a data processing pipeline. The application leverages the power of Rust for high-performance serverless computing along with the workflow orchestration capabilities of Step Functions.

### Key Features
- **Rust AWS Lambda Functions**: Efficient and secure handling of serverless compute tasks using Rust.
- **Step Functions Workflow**: Coordination and management of multiple Lambda functions for a seamless data processing pipeline.
- **Data Processing Pipeline**: Streamlined processing of data with automated tasks triggered at each step.

## Architecture
![Alt text](image.png)

- Step1: calculate the sum of two numbers
- Step2: Calculate the square of the sum from the last step
- Step3: Retrieve the squared sum from the last step and output a message


## Prerequisites
- AWS account
- AWS CLI installed and configured
- Rust environment setup
- Any other dependencies

## Installation and Setup

1. **Clone the repository:**
   ```bash
   git clone https://github.com/your-repo/rust-lambda-step-functions.git
   ```

2. **Deploy the Lambda functions:**
   ```bash
   cargo lambda deploy <your step>
   ```

3. **Set up Step Functions:**
   
   simply drag the lambda function into the step functions, but don't forget to link your lambda function with these steps

## Demo Video
- [Demo Video](https://youtu.be/Yrdm4lu4zuw)

